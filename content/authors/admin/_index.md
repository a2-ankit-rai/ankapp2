---
# Display name
title: Ankit Rai
name: Ankit Rai
# avatar_image: "avatar-2.jpg"
# Username (this should match the folder name)
authors:
- admin
btn:
- url : "files/Resume_Ankit_Rai.pdf"
  label : "Download Resume"

# Is this the primary user of the site?
superuser: true

# Role/position
role: Senior Software Engineer

# Organizations/Affiliations
organizations:
- name: Payconiq
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include highly scalable architechtures and distributed computing.

interests:
- Distributed Computing
- Data engineering
- Application Frameworks

# Should the user's education and interests be displayed?
display_education: true

education:
  courses:
  - course: Bachelor of Engineering (B.E.) in Computer Science
    institution: RGPV University, India
    year: 2012
  # - course: MEng in Artificial Intelligence
  #   institution: Massachusetts Institute of Technology
  #   year: 2009

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/a2ankitrai
- icon: github
  icon_pack: fab
  link: https://github.com/a2ankitrai
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/a2ankitrai/
- icon: stack-overflow
  icon_pack: fab
  link: https://stackoverflow.com/users/5174385/ankit-rai  
- icon: facebook
  icon_pack: fab
  link: https://www.facebook.com/a2.ankitrai
# - icon: file
#   icon_pack: fas
#   link: files/Resume_Ankit_Rai.pdf


# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

I am a Senior Software Engineer at Payconiq in Amsterdam, Netherlands. Currently, I am currently working on developing data engineering solutions for Payment, Merchant and Consumer domain.

Every thing I build is a new challenge to develop something unique and usable. I pay attention to the small details, every pixel matters to me.

Check out my latest application <a href="https://noteshelf.ankitrai.com" target="_blank">NoteShelf</a>. NoteShelf is a smart way to create and manage rich text-based notes. This is an end-to-end application developed with Angular 7 in front-end, hosted on firebase platform and in Spring-Boot REST service at the back-end which is being served along with Database and Cache System from Amazon Web Services.

Other than all of this, I am a Linkin Park fan and Tyler Durden follower.
