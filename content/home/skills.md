+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Skills"
subtitle = "I AM REALLY GOOD AT THE FOLLOWING TECHNICAL SKILLS"

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons

[[feature]]
  icon = "java"
  icon_pack = "custom"
  name = "Java" 

[[feature]]
  icon = "javascript"
  icon_pack = "custom"
  name = "JavaScript" 

[[feature]]
  icon = "kotlinlang"
  icon_pack = "custom"
  name = "Kotlin"   

[[feature]]
  icon = "kafka-2"
  icon_pack = "custom"
  name = "Kafka"    

[[feature]]
  icon = "aws-2"
  icon_pack = "custom"
  name = "AWS" 

[[feature]]
  icon = "spring-3"
  icon_pack = "custom"
  name = "Spring" 

[[feature]]
  icon = "hibernate"
  icon_pack = "custom"
  name = "Hibernate" 

[[feature]]
  icon = "icons8-mongodb"
  icon_pack = "custom"
  name = "Mongo DB" 

[[feature]]
  icon = "amazon-database"
  icon_pack = "custom"
  name = "Relational Databases" 

[[feature]]
  icon = "neo4j"
  icon_pack = "custom"
  name = "Neo4j" 
          
[[feature]]
  icon = "icons8-docker"
  icon_pack = "custom"
  name = "Docker"

[[feature]]
  icon = "icons8-kubernetes"
  icon_pack = "custom"
  name = "Kubernetes"

[[feature]]
  icon = "quarkus"
  icon_pack = "custom"
  name = "Quarkus"
  
  
#[[feature]]
#  icon = "camera-retro"
#  icon_pack = "fas"
#  name = "Photography"
#  description = "10%"

# Uncomment to use emoji icons.
# [[feature]]
#  icon = ":smile:"
#  icon_pack = "emoji"
#  name = "Emojiness"
#  description = "100%"  

# Uncomment to use custom SVG icons.
# Place custom SVG icon in `assets/images/icon-pack/`, creating folders if necessary.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
# [[feature]]
#  icon = "your-custom-icon-name"
#  icon_pack = "custom"
#  name = "Surfing"
#  description = "90%"

+++
