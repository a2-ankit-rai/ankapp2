+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
  title = "Senior Software Engineer"
  company = "Lemonade"
  company_url = "https://www.lemonade.com/nl"
  location = "Amsterdam, Netherlands"
  date_start = "2021-12-01"
  date_end = ""
  description = """

  <!-- Responsibilities include: -->
  
  * Migrating existing monolith code to microservices
  * Developing the next generation AI chatbot
  """

[[experience]]
  title = "Senior Software Engineer"
  company = "Payconiq"
  company_url = "https://www.payconiq.com/"
  location = "Amsterdam, Netherlands"
  date_start = "2019-11-01"
  date_end = "2021-11-30"
  description = """

  <!-- Responsibilities include: -->
  
  * Designed and developed ETL pipeline for Payments, Consumers and Merchants Data
  * Implemented the reactive model of applications improving performance
  * Designed the post transaction monitoring system
  """

[[experience]]
  title = "Software Engineer 2"
  company = "PayPal"
  company_url = "https://www.paypal.com/"
  location = "Bangalore, India"
  date_start = "2017-07-01"
  date_end = "2019-10-31"
  description = """ 

  - Developed and enhanced various microservices in merchants and payments domain.

  - Designed and developed the implementations for Pay-After-Delivery and 3DS (3 Domain Security) feature migration.

  """

[[experience]]
  title = "Software Development Engineer"
  company = "Eaton"
  company_url = "https://www.eaton.com/"
  location = "Pune, India"
  date_start = "2016-01-27"
  date_end = "2017-07-30"
  description = """
  
- Individual contributer for Power Xpert Gateway 900 Dashboard which is the front-end application developed to communicate with Eaton’s electrical devices.

- Developed Configuration Tool module altering the existing implementation from Python to JavaScript which reduced the data access and process time from devices by 40%.

- Implemented an Expression Evaluator API extending Dijkstra’s two stack algorithm to support ternary conditions and unary operations.

  """
  

[[experience]]
  title = "Subject Matter Expert"
  company = "Amdocs"
  company_url = "https://www.amdocs.com/"
  location = "Pune, India"
  date_start = "2015-05-05"
  date_end = "2016-01-25"
  description = """
  
- Individual contributer for SCL which is a suite of mission critical BSS-OSS applications developed for telecom provider Telefonica in Chile, Mexico, Ecuador and Colombia region.

- Developed procedure analyzer application which gave visual representation of the database procedure workflows resulting in reduced analysis time and better maintenance.

- Created UI generation API to perform transition of XML to HTML at runtime leading to dynamic form creation.

"""

[[experience]]
  title = "Systems Engineer"
  company = "Tata Consultancy Services"
  company_url = "https://www.tcs.com/"
  location = "Ahmedabad, India"
  date_start = "2015-05-05"
  date_end = "2016-01-25"
  description = """
  
- Developed core module of the diamond grading application used by the graders at Gemological Institute of America.

- Created an automation tool to retrieve selective updates from SVN into central repository for deployment which led to decrease in the time taken for production deployment by 30%.

"""  

+++
