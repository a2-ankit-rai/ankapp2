+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Certifications"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.


[[item]]
  organization = "VMware"
  organization_url = "https://www.vmware.com/"
  title = "VMware Spring Professional 2021"
  url = ""
  certificate_url = "https://www.credly.com/badges/f27d976d-fbcc-4183-beba-242d55bd6a10"
  date_start = "2021-04-08"
  date_end = ""
  description = ""

[[item]]
  organization = "Oracle"
  organization_url = "https://www.oracle.com/"
  title = "Oracle Certified Professional: Java SE 11 Developer"
  url = ""
  certificate_url = "https://www.youracclaim.com/badges/ab4ee628-4f11-4f2b-bd25-67a3c858c834"
  date_start = "2020-11-09"
  date_end = ""
  description = ""

[[item]]
  organization = "Amazon Web Services"
  organization_url = "https://aws.amazon.com/"
  title = "AWS Certified Solutions Architect – Associate"
  url = "https://aws.amazon.com/certification/certified-solutions-architect-associate/"
  certificate_url = "https://www.youracclaim.com/badges/a43d72b5-e8ae-45d8-a416-6dd21197d715"
  date_start = "2020-07-01"
  date_end = "2023-07-01"
  description = ""

[[item]]
  organization = "Oracle"
  organization_url = "https://www.oracle.com/"
  title = "Oracle Certified Professional, Java SE 6 Programmer"
  url = ""
  certificate_url = "https://www.youracclaim.com/badges/3154fd94-30ea-4d85-a481-480a40a490f5"
  date_start = "2010-11-01"
  date_end = ""
  description = ""

+++
