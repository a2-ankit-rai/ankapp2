---
title: Noteshelf
summary: Smart way to manage notes
tags:
- Angular
- Java
- Spring
date: "2018-09-15T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""
#https://noteshelf.ankitrai.com

image:
  caption:
  focal_point: Smart
---

<p> <a target="_blank" href="https://noteshelf.ankitrai.com">NoteShelf</a> is a smart way to create and manage rich text based notes. Create awesome text notes with an inbuilt editor and manage them online in a safe and secured manner. You can register through your email address or if you just don't want to <a target="_blank" href="https://github.com/a2ankitrai/Password-ProManager">maintain another credentials</a> login via social sign-in platforms is also supported (Currently Google and Github are supported) </p>

<p> Once you are done with registration, you can start creating notes after login. Your saved notes will appear on the Notes section from where new notes can be created and existing notes can be updated or deleted.You can also manage your profile by updating your information in the profile section. </p>

<p> I developed NoteShelf with an intention to build an end to end application integrating various technologies, together which can scale well under load ensuring high availability. There are many components which together power NoteShelf to provide a seamless experience to the user. The NoteShelf <a target="_blank" href="https://github.com/a2ankitrai/NoteShelf-Angular-Client">front-end application</a> is implemented in Angular 7 which communicates with the <a target="_blank" href="https://github.com/a2ankitrai/NoteShelf">back-end service</a> implemented in Java using Spring-Boot Framework. This back-end service communicates with different databases(MongoDB and MySQL) and cache system(Redis) and provide response to user requests in a stateless fashion. NoteShelf also relies on <a target="_blank" href="https://github.com/a2ankitrai/Ank-Oauth2-Client">Ank-Oauth2-Client</a> library(also developed by me) to support sign in from social-login platforms like Google and Github. The front-end application is hosted on firebase platform while the back-end service along with Databases and Cache is hosted on Amazon Web Services. </p>
