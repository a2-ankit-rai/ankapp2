---
title: Password Pro Manager
summary:  Password Manager System
tags: 
- Java
date: "2018-09-15T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: 

image:
  caption:
  focal_point: Smart
---

Password-ProManager is a desktop application to store and manage passwords for different accounts locally.

This application is developed in Java FX and stores the credentials locally on your system. You can create as many credentials as you want.

